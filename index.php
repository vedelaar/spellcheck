<?php

class spellmatch
{
  public $word_node;
  public $error;
  public $str;

  public function __construct($word_node, $error, $str)
  {
    $this->word_node = $word_node;
    $this->error = $error;
    $this->str = $str;
  }

  public function __tostring()
  {
    return str_pad($this->get_word(), 12)." $this->error ".$this->word_node->word_count;
  }

  private function sorter($a, $b)
  {
    if ($a == $b) 
    { 
      return 0; 
    } 
    return ($a < $b) ? -1 : 1;
  }

  public static function sort_on_error_word_count($a, $b)
  {
    if ($a->error == $b->error) 
    { 
      if ($a->word_node->word_count == $b->word_node->word_count) 
      { 
        return 0; 
      } 
      return ($b->word_node->word_count < $a->word_node->word_count) ? -1 : 1;
    } 
    return ($a->error < $b->error) ? -1 : 1;
  }

  public static function sort_on_error($a, $b)
  {
    return $this->sorter($a->error, $b->error);
  }

  public static function sort_on_word_count($a, $b)
  {
    return $this->sorter($a->word_node->word_count, $b->word_node->word_count);
  }

  public function get_word()
  {
    return $this->word_node->get_word();
  }
}

class node
{
  public $subnodes = array();
  public $letter;
  public $is_end = false;
  public $word_count = 0;
  public $parent = null;

  public function get_word()
  {
    if ($this->parent === null)
      return;
    return $this->parent->get_word() . $this->letter;
  }

  public function add_word($leftover, $whole_word = null)
  {
    if (!$whole_word)
      $whole_word = $leftover;

    $letter = $leftover[0];
    $leftover = substr($leftover, 1);

    if (isset($this->subnodes[$letter]))
      $node = $this->subnodes[$letter];
    else
    {
      $node = new node();
      $node->letter = $letter;
      $this->subnodes[$letter] = $node;
      $node->parent = $this;
    }

    if (strlen($leftover) == 0)
    {
      $node->is_end = true;
      $node->word_count += 1;
    }
    else
    {
      $node->add_word($leftover, $whole_word);
    }
  }

  public function find_word($word, $error = 0, $str = '')
  {
    $debug = "word: $word, error: $error this-letter: $this->letter \n";

    //error too big
    if ($error > 2)
      return array();

    //End of word
    if (strlen($word) == 0)
      if ($this->is_end)
        return array(new spellmatch($this, $error, $str));

    //Collect matches
    $matches = array();

    //Remove the current letter and try again
    $leftover = substr($word, 1);
    $matches = array_merge($matches, $this->find_word($leftover, $error+1, $str."find_word substr $leftover ".$debug ));

    //Ask the letter if it's ok and let it handle further
    foreach($this->subnodes as $subnode_letter=>$subnode)
    {
      $matches = array_merge($matches, $subnode->match($word, $error, $str."find_word word $word ".$debug));
    }
    return $this->clean_matches($matches);
  }

  public function match($word, $error, $str = '')
  {
    $debug = "word: $word, error: $error this-letter: $this->letter \n";

    $matches = array();
    $leftover = substr($word, 1);

    if (strlen($word))
    {
      $letter = $word[0];
      if ($this->letter === $letter)
      {
        //all fine
        $matches = array_merge($matches, $this->find_word($leftover, $error, $str."match $this->letter === $letter ".$debug));
        //keep the letter and find new matches, to find walla while typing wala
        $matches = array_merge($matches, $this->find_word($word, $error+1, $str."keep $this->letter === $letter ".$debug));
      }
      else
      {
        //cut the letter away and find new matches
        $matches = array_merge($matches, $this->find_word($leftover, $error+1, $str."match $this->letter !== $letter ".$debug));
        //keep the letter and find new matches to find zwijn while typing zwjn
        $matches = array_merge($matches, $this->find_word($word, $error+1, $str."keep $this->letter !== $letter ".$debug));
      }
    }
    else
      $matches = array_merge($matches, $this->find_word($word, $error+1, $str."keep $this->letter nothing to compare ".$debug));

    return $matches;
  }

  public function clean_matches($matches)
  {
    $ret = array();
    foreach($matches as $match)
      if (!isset($ret[$match->get_word()]) || $ret[$match->get_word()]->error > $match->error)
        $ret[$match->get_word()] = $match;

    usort($ret, array('spellmatch', 'sort_on_error_word_count'));
    return $ret;
  }

  public static function tree_from_dict($file)
  {
    $top = new node();

    $handle = fopen($_SERVER['argv'][1], "r");
    if ($handle) 
    {
      while (($line = fgets($handle)) !== false) {
        $top->add_word(trim($line));
      }
      fclose($handle);
    }
    else
    {
      throw new Exception('error opening file');
    }

    return $top;
  }

  public static function tree_from_file($file)
  {
    $top = new node();

    $handle = fopen($_SERVER['argv'][1], "r");
    if ($handle) 
    {
      while (($line = fgets($handle)) !== false) 
      {
        $words = str_word_count(str_replace('--', ' ',$line),1);
        foreach($words as $word)
        {
          $top->add_word(strtolower(trim($word)));
        }
      }
      fclose($handle);
    }
    else
    {
      throw new Exception('error opening file');
    }

    return $top;
  }

  public function save_tree($file)
  {
    file_put_contents($file, serialize($this));
  }

  public static function load_tree($file)
  {
    return unserialize(file_get_contents($file));
  }
}

//$top = node::tree_from_file($_SERVER['argv'][1]);
$top = node::load_tree($_SERVER['argv'][1]);


//$top = node::tree_from_dict($_SERVER['argv'][1]);

echo "seach for ".$_SERVER['argv'][2]." \n";
//print_r($top->find_word($_SERVER['argv'][2]));
echo implode("\n", $top->clean_matches($top->find_word($_SERVER['argv'][2])));
echo "\n";
